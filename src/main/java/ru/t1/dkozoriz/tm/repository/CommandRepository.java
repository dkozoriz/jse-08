package ru.t1.dkozoriz.tm.repository;

import ru.t1.dkozoriz.tm.api.ICommandRepository;
import ru.t1.dkozoriz.tm.constant.ArgumentConst;
import ru.t1.dkozoriz.tm.constant.CommandConst;
import ru.t1.dkozoriz.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command VERSION = new Command(CommandConst.VERSION, ArgumentConst.VERSION, "show version info.");

    private static final Command ABOUT = new Command(CommandConst.ABOUT, ArgumentConst.ABOUT, "show developer info.");

    private static final Command HELP = new Command(CommandConst.HELP, ArgumentConst.HELP, "show command list.");

    private static final Command INFO = new Command(CommandConst.INFO, ArgumentConst.INFO, "close application.");

    private static final Command EXIT = new Command(CommandConst.EXIT, null, "show system info.");

    private static final Command[] COMMANDS = new Command[] {
            VERSION, ABOUT, HELP, INFO, EXIT
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}